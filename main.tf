provider "aws" {
    region = var.aws_region
}

resource "aws_instance" "Workspace" {
    ami = var.amis
    instance_type = var.aws_instance
    tags = {
      "name" = var.tag_name
    }
}
