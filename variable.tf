variable "amis" {
    type = string
}

variable "aws_region" {
    type = string
}

variable "aws_instance" {
    type = string 
}

variable "tag_name" {
    type = string
}
